package com.mlm.concurrency;

import com.mlm.dao.MemberManagerDao;
import com.mlm.entities.MemberManager;
import com.mlm.enumeration.ResponseStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;

public class MemberManagerThread implements Callable<ResponseStatus>,Comparable<MemberManagerThread> {

    private static final Logger log = LoggerFactory.getLogger(MemberManagerThread.class);

    private static final int SLEEP_TIME = 500;

    private MemberManagerDao memberManagerDao;
    private ConcurrentHashMap<String,MemberManager> concurrentHashMap;
    private MemberManager memberManager;
    private int priority;


    public MemberManagerThread(
            MemberManagerDao memberManagerDao,
            ConcurrentHashMap<String,MemberManager> concurrentHashMap,
            MemberManager memberManager,
            int priority
    ){
        this.memberManagerDao = memberManagerDao;
        this.concurrentHashMap = concurrentHashMap;
        this.memberManager = memberManager;
        this.priority = priority;
    }

    @Override
    public ResponseStatus call() throws Exception {
        log.info("execute callable on callable priority : {}" , this.getPriority());
        ResponseStatus responseStatus;
        try {
            responseStatus = memberManagerDao.execute(concurrentHashMap,memberManager);
            Thread.sleep(SLEEP_TIME);
        } catch (InterruptedException e) {
            e.printStackTrace();
            return ResponseStatus.ERROR;
        }
        return responseStatus;
    }

    @Override
    public int compareTo(MemberManagerThread o) {
        return Integer.compare(o.getPriority(), this.getPriority());
    }

    private int getPriority(){
        return priority;
    }





}
