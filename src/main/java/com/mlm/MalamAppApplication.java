package com.mlm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MalamAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(MalamAppApplication.class, args);
	}
}
