package com.mlm.dao;

import com.mlm.entities.MemberManager;
import com.mlm.enumeration.ResponseStatus;

import java.util.concurrent.ConcurrentHashMap;

public interface MemberManagerDao {

    ResponseStatus execute(ConcurrentHashMap<String,MemberManager> concurrentHashMap, MemberManager memberManager);
}
