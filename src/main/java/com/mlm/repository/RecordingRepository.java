package com.mlm.repository;

import com.mlm.entities.Recording;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RecordingRepository extends JpaRepository<Recording,Long> {

}
