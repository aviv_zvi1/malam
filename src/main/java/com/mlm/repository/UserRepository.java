package com.mlm.repository;

import com.mlm.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User,Long> {

//    @Query("select u from User u where u.userName = ?#{principal.username}")
//    Optional<User> getCurrentUser();

    Optional<User> findByUserName(String userName);

}
