package com.mlm.service;


import com.mlm.entities.User;
import com.mlm.entities.dto.UserAppDto;
import com.mlm.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {

    private UserRepository userRepository;

    public UserService (UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<UserAppDto> getAllUsersWithCounters(){
        return userRepository.findAll().stream().map(UserAppDto::new).collect(Collectors.toList());
    }

//    normally we will work with spring security and principle to get the current user ,
//    the method implementation can be found in user repository .
    Optional<User> getCurrentUser() {
        return userRepository.findByUserName("aviv");
    }

}
