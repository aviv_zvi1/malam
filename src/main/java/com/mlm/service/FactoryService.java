package com.mlm.service;


import com.mlm.dao.MemberManagerDao;
import com.mlm.enumeration.CrudEnum;
import com.mlm.facade.*;
import org.springframework.stereotype.Service;

@Service
public class FactoryService {

    MemberManagerDao getFactory(CrudEnum crudEnum){

        switch (crudEnum) {
            case CREATE:
                return new CreateService();
            case UPDATE:
                return new UpdateService();
            case DELETE:
                return new DeleteService();
        }
        return null;
    }
}
