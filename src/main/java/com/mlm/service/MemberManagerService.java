package com.mlm.service;

import com.mlm.concurrency.MemberManagerThread;
import com.mlm.configuration.ThreadPoolConfig;
import com.mlm.entities.MemberManager;
import com.mlm.entities.User;
import com.mlm.enumeration.CrudEnum;
import com.mlm.enumeration.ResponseStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.*;
import java.util.stream.Collectors;

@Service
public class MemberManagerService {

    private static final Logger log = LoggerFactory.getLogger(MemberManagerService.class);

    private static final ConcurrentHashMap<String,MemberManager> MEMBER_MANAGER_CONCURRENT_HASH_MAP = new ConcurrentHashMap<>();

    private FactoryService factoryService;

    private PriorityService priorityService;

    private RecordingService recordingService;

    private ThreadPoolConfig threadPoolConfig;

    private UserService userService;

    public MemberManagerService (
        FactoryService factoryService ,
        PriorityService priorityService,
        RecordingService recordingService,
        ThreadPoolConfig threadPoolConfig,
        UserService userService
        ) {
        this.factoryService = factoryService;
        this.priorityService = priorityService;
        this.recordingService = recordingService;
        this.threadPoolConfig = threadPoolConfig;
        this.userService = userService;
    }


    public MemberManager executeTask(MemberManager memberManager, CrudEnum crudEnum) throws ExecutionException, InterruptedException {
        log.info("MemberManagerService : thread service insert to thread pool member manager : {}" , memberManager);

        Optional<User> user = userService.getCurrentUser();

        if(user.isPresent()) {

            MemberManagerThread memberManagerThread = new MemberManagerThread(
                    factoryService.getFactory(crudEnum)
                    , MEMBER_MANAGER_CONCURRENT_HASH_MAP,
                    memberManager,
                    priorityService.getPriority(crudEnum)
            );

            Future<ResponseStatus> future = threadPoolConfig.getThreadPool().submit(memberManagerThread);

            if (future.get().equals(ResponseStatus.SUCCESS)) {
                recordingService.insertRecordToDb(crudEnum, user.get());
            }

        }

        checkForShutDown();

        return memberManager;
    }

    public List<MemberManager> getList(){
        return MEMBER_MANAGER_CONCURRENT_HASH_MAP.entrySet().stream().map(Map.Entry::getValue).collect(Collectors.toList());
    }

    private void checkForShutDown(){
        if(MEMBER_MANAGER_CONCURRENT_HASH_MAP.isEmpty()){
            ThreadPoolExecutor executor =  threadPoolConfig.getThreadPool();
            try {
                executor.shutdown();
                executor.awaitTermination(1, TimeUnit.DAYS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
