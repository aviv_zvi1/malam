package com.mlm.service;

import com.mlm.enumeration.CrudEnum;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicInteger;

@Service
public class PriorityService {

    private final AtomicInteger insert = new AtomicInteger(0);

    private final AtomicInteger remove = new AtomicInteger(1);


    int getPriority(CrudEnum crudEnum){
        if(crudEnum.equals(CrudEnum.CREATE)){
            return insert.incrementAndGet();
        }else {
            return remove.addAndGet(2);
        }
    }
}
