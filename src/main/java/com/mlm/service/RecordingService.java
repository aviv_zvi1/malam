package com.mlm.service;

import com.mlm.entities.Recording;
import com.mlm.entities.User;
import com.mlm.entities.dto.UserAppDto;
import com.mlm.enumeration.CrudEnum;
import com.mlm.repository.RecordingRepository;
import com.mlm.repository.UserRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

@Service
public class RecordingService {

    private static final String GET_MAX_RECORD_BY_USER = "SELECT user_id, COUNT(*) as con FROM recording GROUP BY user_id ORDER by con DESC LIMIT 1";

    private RecordingRepository recordingRepository;

    private UserRepository userRepository;

    private JdbcTemplate jdbcTemplate;

    public RecordingService (RecordingRepository recordingRepository,UserRepository userRepository,JdbcTemplate jdbcTemplate) {
        this.recordingRepository = recordingRepository;
        this.userRepository = userRepository;
        this.jdbcTemplate = jdbcTemplate;
    }

    public UserAppDto getUserWithMaxRecords(){
        QueryResponse res = findUserWithMaxRecords();
        if(res != null){
            Optional<User> user = userRepository.findById(Long.valueOf(res.getId()));
            return user.map(user1 -> new UserAppDto(user1, res.getCount())).orElse(null);
        }
        return null;
    }

    @Async
    void insertRecordToDb(CrudEnum crudEnum, User user){
        recordingRepository.save(new Recording(crudEnum, user));
    }

    private QueryResponse findUserWithMaxRecords() {

        return (QueryResponse) jdbcTemplate.queryForObject(GET_MAX_RECORD_BY_USER, new QueryResponse());
    }

    class QueryResponse implements RowMapper {

        private Integer id;
        private Integer count;

        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            QueryResponse queryResponse = new QueryResponse();
            queryResponse.setId(rs.getInt(1));
            queryResponse.setCount(rs.getInt(2));
            return queryResponse;
        }

        Integer getId() {
            return id;
        }

        void setId(Integer id) {
            this.id = id;
        }

        Integer getCount() {
            return count;
        }

        void setCount(Integer count) {
            this.count = count;
        }

    }

}
