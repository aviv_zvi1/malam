package com.mlm.entities.dto;

import com.mlm.entities.User;

public class UserAppDto extends User {

    private int recordingCounter;

    public UserAppDto (User user){
        super(user);
        this.recordingCounter = user.getRecordings().size();
    }

    public UserAppDto (User user, int counter){
        super(user);
        this.recordingCounter = counter;
    }

    public UserAppDto() {
    }

    public int getRecordingCounter() {
        return recordingCounter;
    }

    public void setRecordingCounter(int recordingCounter) {
        this.recordingCounter = recordingCounter;
    }
}
