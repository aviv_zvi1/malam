package com.mlm.entities;


public class MemberManager {

    private String id;
    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MemberManager manager = (MemberManager) o;

        if (id != null ? !id.equals(manager.id) : manager.id != null) return false;
        return description != null ? description.equals(manager.description) : manager.description == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "MemberManager{" +
                "id='" + id + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
