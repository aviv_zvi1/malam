package com.mlm.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.mlm.enumeration.CrudEnum;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

@Entity
@Table(name = "recording")
public class Recording {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "created_date")
    private ZonedDateTime createdDate = ZonedDateTime.now();

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "crud_enum", nullable = false)
    private CrudEnum crudEnum;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public Recording() {
    }

    public Recording(CrudEnum crudEnum, User user) {
        this.crudEnum = crudEnum;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public CrudEnum getCrudEnum() {
        return crudEnum;
    }

    public void setCrudEnum(CrudEnum crudEnum) {
        this.crudEnum = crudEnum;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
