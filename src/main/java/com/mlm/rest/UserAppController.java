package com.mlm.rest;


import com.mlm.entities.User;
import com.mlm.entities.dto.UserAppDto;
import com.mlm.repository.RecordingRepository;
import com.mlm.repository.UserRepository;
import com.mlm.service.RecordingService;
import com.mlm.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserAppController {

    private static final Logger log = LoggerFactory.getLogger(UserAppController.class);

    private UserRepository userRepository;

    private UserService userService;

    private RecordingService recordingService;

    public UserAppController (UserRepository userRepository,UserService userService,RecordingService recordingService) {
        this.userRepository = userRepository;
        this.userService = userService;
        this.recordingService = recordingService;
    }


    @PostMapping
    public void createUser(@RequestBody User user){
        userRepository.save(user);
    }

    @GetMapping("/fetch-all-with-counter")
    public List<UserAppDto> getAllUsersWithCounters(){
        log.info("fetch all users with records counters");
        return userService.getAllUsersWithCounters();
    }

    @GetMapping("get-user-with-max-records")
    public UserAppDto getUserWithMaxRecords(){
        log.info("fetch user with most records");
        return recordingService.getUserWithMaxRecords();
    }

}
