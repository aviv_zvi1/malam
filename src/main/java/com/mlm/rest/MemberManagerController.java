package com.mlm.rest;


import com.mlm.entities.MemberManager;
import com.mlm.enumeration.CrudEnum;
import com.mlm.service.MemberManagerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("member-manager")
public class MemberManagerController {

    private static final Logger log = LoggerFactory.getLogger(MemberManagerController.class);

    private MemberManagerService memberManagerService;

    public MemberManagerController (MemberManagerService memberManagerService) {
        this.memberManagerService = memberManagerService;
    }

    @PostMapping
    public MemberManager createMemberManager(@RequestBody MemberManager memberManager) throws ExecutionException, InterruptedException {
        log.info("MemberManagerController : create a member manager : {} " , memberManager);
        return memberManagerService.executeTask(memberManager, CrudEnum.CREATE);
    }

    @PutMapping
    public MemberManager updateMemberManager(@RequestBody MemberManager memberManager) throws ExecutionException, InterruptedException {
        log.info("MemberManagerController : update a member manager : {} " , memberManager);
        return memberManagerService.executeTask(memberManager, CrudEnum.UPDATE);
    }

    @DeleteMapping
    public MemberManager deleteMemberManager(@RequestBody MemberManager memberManager) throws ExecutionException, InterruptedException {
        log.info("MemberManagerController : delete a member manager : {} " , memberManager);
        return memberManagerService.executeTask(memberManager, CrudEnum.DELETE);
    }

    @GetMapping
    public List<MemberManager> getAllMemberManagers(){
        return memberManagerService.getList();
    }

}
