package com.mlm.rest;


import com.mlm.entities.Recording;
import com.mlm.repository.RecordingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/recording")
public class RecordingController {

    private static final Logger log = LoggerFactory.getLogger(RecordingController.class);

    private RecordingRepository recordingRepository;

    public RecordingController (RecordingRepository recordingRepository) {
        this.recordingRepository = recordingRepository;
    }

    @GetMapping
    public List<Recording> getAll(){
        log.info("RecordingController : get all records");
        return recordingRepository.findAll();
    }

}
