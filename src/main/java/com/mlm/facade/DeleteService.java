package com.mlm.facade;

import com.mlm.dao.MemberManagerDao;
import com.mlm.entities.MemberManager;
import com.mlm.enumeration.ResponseStatus;

import java.util.concurrent.ConcurrentHashMap;

public class DeleteService implements MemberManagerDao{

    @Override
    public ResponseStatus execute(ConcurrentHashMap<String, MemberManager> concurrentHashMap, MemberManager memberManager) {
        if(concurrentHashMap.contains(memberManager)){
            concurrentHashMap.remove(memberManager.getId());
            return ResponseStatus.SUCCESS;
        }
        return ResponseStatus.ERROR;
    }

}
