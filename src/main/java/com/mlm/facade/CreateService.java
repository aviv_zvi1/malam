package com.mlm.facade;

import com.mlm.dao.MemberManagerDao;
import com.mlm.entities.MemberManager;
import com.mlm.enumeration.ResponseStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class CreateService implements MemberManagerDao {

    private static final Logger log = LoggerFactory.getLogger(CreateService.class);

    @Override
    public ResponseStatus execute(ConcurrentHashMap<String, MemberManager> concurrentHashMap, MemberManager memberManager) {
        log.info("create service creating member manager : {}" , memberManager);
        String primaryKey = UUID.randomUUID().toString();
        memberManager.setId(primaryKey);
        concurrentHashMap.put(primaryKey,memberManager);
        return ResponseStatus.SUCCESS;
    }
}
