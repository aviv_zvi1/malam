package com.mlm.facade;

import com.mlm.dao.MemberManagerDao;
import com.mlm.entities.MemberManager;
import com.mlm.enumeration.ResponseStatus;

import java.util.concurrent.ConcurrentHashMap;

public class UpdateService implements MemberManagerDao {

    @Override
    public ResponseStatus execute(ConcurrentHashMap<String, MemberManager> concurrentHashMap, MemberManager memberManager) {
        MemberManager manager = concurrentHashMap.get(memberManager.getId());
        manager.setDescription(memberManager.getDescription());
        return ResponseStatus.SUCCESS;
    }
}
