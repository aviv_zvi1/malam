package com.mlm.service;

import com.mlm.entities.dto.UserAppDto;
import com.mlm.repository.RecordingRepository;
import com.mlm.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.concurrent.ExecutionException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MemberManagerServiceTest {

    private static final Logger log = LoggerFactory.getLogger(MemberManagerServiceTest.class);

    @Inject
    @InjectMocks
    private UserService userService;

    @Inject
    @InjectMocks
    private RecordingService recordingService;

    @Mock
    RecordingRepository recordingRepository;

    @Mock
    UserRepository userRepository;


    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @Transactional
    public void getUserWithCounters() throws ExecutionException, InterruptedException {

        userService.getAllUsersWithCounters().forEach(this::printIntoLog);
    }

    @Test
    public void getUserWithMaxCounters() throws ExecutionException, InterruptedException {

        UserAppDto records = recordingService.getUserWithMaxRecords();
        printIntoLog(records);
    }


    private void printIntoLog(UserAppDto user){
        log.info("records counting : {} , by user name : {}" , user.getRecordingCounter() , user.getUserName());
    }

}
